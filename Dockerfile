# This file is a template, and might need editing before it works on your project.
#FROM node:8.0

#WORKDIR /usr/src/app

#ARG NODE_ENV
#ENV NODE_ENV $NODE_ENV
#COPY package.json /usr/src/app/
#RUN npm install && npm cache clean
#COPY . /usr/src/app

#CMD [ "npm", "start" ]

# replace this with your application's default port
#EXPOSE 9000
FROM node:8.0

# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json /usr/src/app/

RUN npm install && npm cache clean
# If you are building your code for production
# RUN npm install --only=production

# Bundle app source
COPY . .

EXPOSE 9000
CMD [ "npm", "start" ]
